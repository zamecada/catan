module cz.fel.cvut.pjv {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.base;
    requires javafx.graphics;

    opens cz.fel.cvut.pjv to javafx.fxml;
    opens cz.fel.cvut.pjv.controller to javafx.fxml;
    
    exports cz.fel.cvut.pjv; // Export the main package
    exports cz.fel.cvut.pjv.view; // Export the view package
    exports cz.fel.cvut.pjv.controller; // Export the controller package
}

