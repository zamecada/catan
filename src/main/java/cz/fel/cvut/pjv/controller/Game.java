package cz.fel.cvut.pjv.controller;

import java.util.ArrayList;
import java.util.List;

import cz.fel.cvut.pjv.model.Board;
import cz.fel.cvut.pjv.model.Dice;
import cz.fel.cvut.pjv.model.GameState;
import cz.fel.cvut.pjv.model.Intersection;
import cz.fel.cvut.pjv.model.Player;
import cz.fel.cvut.pjv.model.Terrain;
import cz.fel.cvut.pjv.model.Tile;
import cz.fel.cvut.pjv.view.BoardUI;

public class Game {
    
    private Board board;
    private BoardUI boardUI;
    private Player player1;
    private Player player2;
    private int pointsToWin, gameRound;
    private GameState gameState;
    private Dice dice;
    
    public Game(String player1Name, String player2Name, int pointsToWin, Terrain[] tileTerrains, int[] activationNumbers) {
        this.player1 = new Player(player1Name);
        this.player2 = new Player(player2Name);
        this.pointsToWin = pointsToWin;
        this.gameRound = 0;
        this.board = new Board();
        this.gameState = GameState.SETUP;
        this.dice = new Dice();
        board.initializeTiles(tileTerrains, activationNumbers);
    }

    public void setBoardUI(BoardUI boardUI) {
        this.boardUI = boardUI;
    }
    
    public Board getBoard() {
        return board;
    }

    public List<Player> getPlayers(){
        List<Player> players = new ArrayList<>();
        players.add(player1);
        players.add(player2);
        return players;
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public int getPointsToWin() {
        return pointsToWin;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public int getGameRound() {
        return gameRound;
    }

    public Dice getDice() {
        return dice;
    }

    public void addResourcesToPlayers(Integer outcome) {
        for (Tile tile : board.getTiles()) {
            if (tile.getActivationNumber() == outcome) {
                for (Intersection intersection : tile.getBuildPoints()) {
                    if (intersection.getStructure() != null) {
                        Player player = intersection.getStructure().getPlayer();
                        player.addResource(tile.getTerrain().getResource(), intersection.getStructure().getResourceMultiplier());
                    }
                }
            }
        }
    }

    public boolean isGameOver() {
        if (player1.getPoints() >= pointsToWin || player2.getPoints() >= pointsToWin) {
            return true;
        }
        return false;
    }


}
