package cz.fel.cvut.pjv.controller;

import cz.fel.cvut.pjv.model.Terrain;
import cz.fel.cvut.pjv.view.BoardUI;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class GameSetup {

    @FXML
    private TextField player1Field, player2Field, pointsToWinField;

    @FXML
    private Label error;

    @FXML
    private ChoiceBox<String> ActivationNumbers, Terrains;

    @FXML
    private void startNewGame() {

        String player1Name = player1Field.getText();
        String player2Name = player2Field.getText();
        String pointsToWin = pointsToWinField.getText();
        String activationNumber = ActivationNumbers.getValue();
        String terrain = Terrains.getValue();


        if(player1Name.isEmpty() || player2Name.isEmpty() || pointsToWin.isEmpty() || activationNumber == null || terrain == null){
            error.setVisible(true);
            error.setText("Please fill in all values.");
            System.out.println("Please fill in all values.");
            return;
        }
        if(player1Name.equals(player2Name)) {
            error.setVisible(true);
            error.setText("Player names must be different.");
            System.out.println("Player names must be different.");
            return;
        }
        if (!pointsToWin.matches("\\d+")){
            error.setVisible(true);
            error.setText("Winning points must be a number.");
            System.out.println("Winning points must be a number.");
            return;
        } 
        
        int pointsToWinInt = Integer.parseInt(pointsToWin);
        
        int[] activationNumbers = getActivationNumbers(activationNumber);
        Terrain[] tileTerrains = getTerrains(terrain);

        try {
            // Create an instance of GameController and pass attributes to it
            Game game = new Game(player1Name, player2Name, pointsToWinInt, tileTerrains, activationNumbers);

            // Load the FXML file for the game board UI
            FXMLLoader loader = new FXMLLoader(getClass().getResource("GameBoard.fxml"));
            Parent root = loader.load();

            // Initialize GameboardUI controller
            BoardUI boardUI = loader.getController();
            boardUI.setGame(game); // Pass GameController instance to GameboardUI

            game.setBoardUI(boardUI); // Pass GameboardUI instance to GameController

            // Close the current window (GameSetup.fxml)
            Stage currentStage = (Stage) player1Field.getScene().getWindow();
            currentStage.close();

            // Open GameBoard.fxml in a new window
            Stage gameBoardStage = new Stage();
            Scene scene = new Scene(root);
            gameBoardStage.setScene(scene);
            gameBoardStage.setTitle("Game Board");
            gameBoardStage.show();

        } catch (IOException e) {
            e.printStackTrace(); // Handle the exception appropriately
        }

        System.out.println("Starting game with Player 1: " + player1Name + ", Player 2: " + player2Name +
                ", Winning Points: " + pointsToWin);
    }

    private int[] getActivationNumbers(String method){
        int[] activationNumbers = new int[19];
        if(method.equals("Default")){
            activationNumbers = new int[]{10, 2, 9, 12, 6, 4, 10, 9, 11, 7, 3, 8, 8, 3, 4, 5, 5, 6, 11};
        } else if(method.equals("Random")){
            activationNumbers = new int[]{2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12};
            for(int i = 0; i < activationNumbers.length; i++){
                int randomIndex = (int) (Math.random() * activationNumbers.length);
                int temp = activationNumbers[i];
                activationNumbers[i] = activationNumbers[randomIndex];
                activationNumbers[randomIndex] = temp;
            }
        }
        //TODO: implement custom activation numbers
        return activationNumbers;
    }

    private Terrain[] getTerrains(String method){
        Terrain[] tileTerrains = new Terrain[19];
        if(method.equals("Default")){
            tileTerrains = new Terrain[]{Terrain.MOUNTAINS, Terrain.PASTURE, Terrain.FOREST, Terrain.FIELDS, Terrain.HILLS, Terrain.PASTURE, Terrain.HILLS, Terrain.FIELDS, Terrain.FOREST, Terrain.DESERT, Terrain.FOREST, Terrain.MOUNTAINS, Terrain.FOREST, Terrain.MOUNTAINS, Terrain.FIELDS, Terrain.PASTURE, Terrain.HILLS, Terrain.FIELDS, Terrain.PASTURE};
        } else if(method.equals("Random")){
            tileTerrains = new Terrain[]{Terrain.PASTURE, Terrain.PASTURE, Terrain.PASTURE, Terrain.PASTURE, Terrain.FIELDS, Terrain.FIELDS, Terrain.FIELDS, Terrain.FIELDS, Terrain.FOREST, Terrain.FOREST, Terrain.FOREST, Terrain.FOREST, Terrain.HILLS, Terrain.HILLS, Terrain.HILLS, Terrain.MOUNTAINS, Terrain.MOUNTAINS, Terrain.MOUNTAINS, Terrain.DESERT};
            for(int i = 0; i < tileTerrains.length; i++){
                int randomIndex = (int) (Math.random() * tileTerrains.length);
                Terrain temp = tileTerrains[i];
                tileTerrains[i] = tileTerrains[randomIndex];
                tileTerrains[randomIndex] = temp;
            }
        }
        //TODO: implement custom terrains
        return tileTerrains;
    }
}

