package cz.fel.cvut.pjv.controller;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainMenu {

    public void launchGameSetup() {
        try {
            // Load the GameSetup.fxml file
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/cz/fel/cvut/pjv/GameSetup.fxml"));
            Parent root = loader.load();

            // Create a new stage for the game setup screen
            Stage gameSetupStage = new Stage();
            gameSetupStage.initModality(Modality.APPLICATION_MODAL);
            gameSetupStage.setTitle("Game Setup");
            gameSetupStage.setScene(new Scene(root));

            // Show the game setup screen
            gameSetupStage.showAndWait(); // Use showAndWait to wait for this stage to close before continuing
        } catch (IOException e) {
            e.printStackTrace(); // Handle potential FXML loading exception
        }
    }

    public void functionToLoadGame() {
        //will open a screen with a button to choose a saved game
    }

    public void functionToShowBoardBuilder() {
        //will open a tool to create custom board
    }


    public void functionToExitTheApplication() {
        // will close the game
    }
    
}
