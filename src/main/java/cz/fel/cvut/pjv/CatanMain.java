package cz.fel.cvut.pjv;
 
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
 
public class CatanMain extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        // Load main menu scene
        FXMLLoader menuLoader = new FXMLLoader(getClass().getResource("MainMenu.fxml"));
        Scene mainMenuScene = new Scene(menuLoader.load(), 1920, 1080);

        // Set main menu scene
        primaryStage.setScene(mainMenuScene);
        primaryStage.setTitle("Catan Game"); // Set window title
        primaryStage.setResizable(false); // Prevent the window from being resized
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args); // Launch the JavaFX application
    }
}
