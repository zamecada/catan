package cz.fel.cvut.pjv.model;

import java.util.ArrayList;
import java.util.List;

public class Tile{

    private Terrain terrain;
    private int activationNumber;
    private List<Intersection> intersections;

    public Tile() {
        this.intersections = new ArrayList<>();
    }
    
    public Tile(int activationNumber, List<Intersection> intersections) {
        this.activationNumber = activationNumber;
        this.intersections = intersections;
    }

    public int getActivationNumber() {
        return activationNumber;
    }

    public void setActivationNumber(int activationNumber) {
        this.activationNumber = activationNumber;
    }

    public Terrain getTerrain() {
        return terrain;
    }

    public void setTerrain(Terrain terrain) {
        this.terrain = terrain;
    }

    public List<Intersection> getBuildPoints() {
        return intersections;
    }

    public void setBuildPoints(List<Intersection> intersections) {
        this.intersections = intersections;
    }
}
