package cz.fel.cvut.pjv.model;

import java.util.Random;
public class Dice  {
    Random r;
    public Dice(){
        r = new Random();
    }
    public int[] roll(){
        int i = r.nextInt(6) + 1;
        int j = r.nextInt(6) + 1;
        int[] arr = {i,j}; //separate instead of sum to draw them
        return arr;
    }
}
