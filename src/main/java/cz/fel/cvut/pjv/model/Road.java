package cz.fel.cvut.pjv.model;

import java.util.Set;

public class Road {
    private Set<Intersection> ends;
    private Player player;

    public Road() {
    }

    public Road(Set<Intersection> intersections) {
        this.ends = intersections;
        this.player = null;
    }

    public Road(Set<Intersection> intersections, Player player) {
        this.ends = intersections;
        this.player = player;
    }
    
    public Set<Intersection> getEnds() {
        return ends;
    }

    public Player getPlayer() {
        return player;
    }

    public void setEnds(Set<Intersection> ends) {
        this.ends = ends;
    }
}
