package cz.fel.cvut.pjv.model;

import java.util.ArrayList;
import java.util.Set;

public class Intersection {

    private BuildStructure structure;
    private Set<Road> roads;
    private Intersection left;
    private Intersection right;
    private Intersection vertical;

    public Intersection() {
    }

    public void setNeighbours(Intersection left, Intersection right, Intersection vertical) {
        this.left = left;
        this.right = right;
        this.vertical = vertical;
    }

    /**
     * Check if the structure is empty.
     *
     * @return         true if the structure is empty, false otherwise
     */
    public boolean isEmpty() {
        return this.structure == null;
    }

    public void buildStructure(BuildStructure structure) {
        this.structure = structure;
    }

    public BuildStructure getStructure(){
        return this.structure;
    }

    
    
    public Set<Road> getRoads() {
        return roads;
    }

    public void setRoads(Set<Road> roads) {
        this.roads = roads;
    }

    public void addRoad(Road road) {
        this.roads.add(road);
    }

    /**
     * Gets the value of the right neighbour id
     *
     * @return         the value of the right neighbour id
     */
    public Intersection getRight() {
        return right;
    }

    /**
     * Set the value of the 'right' neighbour id
     *
     * @param  right  the new value for the 'right' neighbour id
     */
    public void setRight(Intersection right) {
        this.right = right;
    }

    /**
     * Gets the value of the left neighbour id
     *
     * @return         the value of the left neighbour id
     */
    public Intersection getLeft() {
        return left;
    }

    /**
     * Sets the value of the left neighbour id
     *
     * @param  left    the new value for the left neighbour id
     */
    public void setLeft(Intersection left) {
        this.left = left;
    }

    /**
     * Gets the value of the vertical neighbour id
     *
     * @return         the value of the vertical neighbour id
     */
    public Intersection getVertical() {
        return vertical;
    }

    /**
     * Sets the value of the vertical neighbour id
     *
     * @param  vertical   the new the value for the vertical neighbour id
     */
    public void setVertical(Intersection vertical) {
        this.vertical = vertical;
    }
}
