package cz.fel.cvut.pjv.model;

public enum GameState {
    SETUP,
    ROLL,
    BUILD,
    GAME_OVER;
}
