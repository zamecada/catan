package cz.fel.cvut.pjv.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Player {

    private final String name;
    private int points;
    private Map<String, Integer> resources;

    private static final int MAX_VILLAGES = 5;
    private static final int MAX_TOWNS = 4;
    private static final int MAX_ROADS = 15;
    // Available structures (how many the player can build)
    private Map<String, Integer> availableStructures;
    // Built structures
    private List<BuildStructure> builtStructures;

    public Player(String name) {
        this.name = name;
        this.points = 0;

        this.resources = new HashMap<>();
        initializeResources();

        this.availableStructures = new HashMap<>();
        this.builtStructures = new ArrayList<>();
        initializeStructures();
    }

    public String getName() {
        return this.name;
    }

    public int getPoints() {
        return this.points;
    }

    // Method to initialize structures list
    private void initializeStructures() {
        // Initialize available structures with maximum limits
        this.availableStructures.put("Village", MAX_VILLAGES);
        this.availableStructures.put("Town", MAX_TOWNS);
        this.availableStructures.put("Road", MAX_ROADS);
    }

    // Method to initialize resources map
    private void initializeResources() {
        // Define resource strings
        String[] resourceTypes = {"Lumber", "Wool", "Grain", "Ore", "Brick", "Nothing"};

        // Initialize each resource type with value 0
        for (String resource : resourceTypes) {
            this.resources.put(resource, 0);
        }
    }

    /**
     * Adds the specified amount of a given resource type to the resources map.
     *
     * @param  resourceType    the type of resource to add
     * @param  amount          the amount of the resource to add
     */
    public void addResource(String resourceType, int amount) {
        if (this.resources.containsKey(resourceType)) {
            int currentAmount = this.resources.get(resourceType);
            this.resources.put(resourceType, currentAmount + amount);
        } else {
            System.out.println("Invalid resource type!");
        }
    }

    /**
     * Removes the specified amount of a given resource type from the resources map.
     *
     * @param  resourceType    the type of resource to remove
     * @param  amount          the amount of the resource to remove
     */
    public void removeResource(String resourceType, int amount) {
        if (this.resources.containsKey(resourceType)) {
            int currentAmount = this.resources.get(resourceType);
            this.resources.put(resourceType, currentAmount - amount);
        } else {
            System.out.println("Invalid resource type!");
        }
    }

    /**
     * Returns the amount of a given resource type in the resources map.
     *
     * @param  resourceType    the type of resource to get
     * @return                 the amount of the resource
     */
    public int getResource(String resourceType) {
        if (this.resources.containsKey(resourceType)) {
            return this.resources.get(resourceType);
        } else {
            System.out.println("Invalid resource type!");
            return -1;
        }
    }
}   
