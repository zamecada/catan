package cz.fel.cvut.pjv.model;

public enum Terrain {
    DESERT,
    FIELDS,
    PASTURE,
    FOREST,
    HILLS,
    MOUNTAINS;      

    public String getResource() {
        switch (this) {
            case DESERT:
                return "None";
            case FIELDS:
                return "grain";
            case PASTURE:
                return "wool";
            case FOREST:
                return "lumber";
            case HILLS:
                return "brick";
            case MOUNTAINS:
                return "ore";
            default:
                throw new IllegalArgumentException("Unexpected value: " + this);
        }
    }
}
