package cz.fel.cvut.pjv.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Board {
    private List<Tile> tiles;
    private List<Intersection> intersections;
    private List<Road> roads;

    public Board() {
        intersections = new ArrayList<Intersection>();
        for (int i = 0; i < 54; i++) {
            intersections.add(new Intersection()); //adds 54 intersections
        }
        assignNeighboursToIntersections(); //sets right, left and vertical neighbours for each intersection
        
        roads = new ArrayList<Road>();
        assignEndsToRoads(); //adds 72 roads and sets ends for each road

        tiles = new ArrayList<>();
        assignIntersectionsToTiles(); //adds 19 tiles and sets intersections for each tile
    }

    /**
     * Sets the terrains and activation numbers for each tile on the board.
     *
     * @param tileTerrains      an array of Terrain objects representing the terrains for each tile
     * @param activationNumbers an array of integers representing the activation numbers for each tile
     */
    public void initializeTiles(Terrain[] tileTerrains, int[] activationNumbers){
        for (int i = 0; i < tiles.size(); i++) {
            tiles.get(i).setTerrain(tileTerrains[i]);
            tiles.get(i).setActivationNumber(activationNumbers[i]);
        }
    }

    public List<Tile> getTiles() {
        return tiles;
    }

    public List<Intersection> getIntersections() {
        return intersections;
    }

    public List<Road> getRoads() {
        return roads;
    }


    /**
     * Assigns intersections to tiles.
     * This method creates tiles and assigns intersections to them based on a predefined pattern.
     */
    public void assignIntersectionsToTiles() {
        tiles.add(new Tile(0, Arrays.asList(intersections.get(0), intersections.get(1), intersections.get(2), intersections.get(8), intersections.get(9), intersections.get(10))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(2), intersections.get(3), intersections.get(4), intersections.get(10), intersections.get(11), intersections.get(12))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(4), intersections.get(5), intersections.get(6), intersections.get(12), intersections.get(13), intersections.get(14))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(7), intersections.get(8), intersections.get(9), intersections.get(17), intersections.get(18), intersections.get(19))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(9), intersections.get(10), intersections.get(11), intersections.get(19), intersections.get(20), intersections.get(21))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(11), intersections.get(12), intersections.get(13), intersections.get(21), intersections.get(22), intersections.get(23))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(13), intersections.get(14), intersections.get(15), intersections.get(23), intersections.get(24), intersections.get(25))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(16), intersections.get(17), intersections.get(18), intersections.get(27), intersections.get(28), intersections.get(29))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(18), intersections.get(19), intersections.get(20), intersections.get(29), intersections.get(30), intersections.get(31))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(20), intersections.get(21), intersections.get(22), intersections.get(31), intersections.get(32), intersections.get(33))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(22), intersections.get(23), intersections.get(24), intersections.get(33), intersections.get(34), intersections.get(35))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(24), intersections.get(25), intersections.get(26), intersections.get(35), intersections.get(36), intersections.get(37))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(28), intersections.get(29), intersections.get(30), intersections.get(38), intersections.get(39), intersections.get(40))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(30), intersections.get(31), intersections.get(32), intersections.get(40), intersections.get(41), intersections.get(42))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(32), intersections.get(33), intersections.get(34), intersections.get(42), intersections.get(43), intersections.get(44))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(34), intersections.get(35), intersections.get(36), intersections.get(44), intersections.get(45), intersections.get(46))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(39), intersections.get(40), intersections.get(41), intersections.get(47), intersections.get(48), intersections.get(49))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(41), intersections.get(42), intersections.get(43), intersections.get(49), intersections.get(50), intersections.get(51))));
        tiles.add(new Tile(0, Arrays.asList(intersections.get(43), intersections.get(44), intersections.get(45), intersections.get(51), intersections.get(52), intersections.get(53))));
    }

    /**
     * Assigns neighbours to intersections.
     * This method assigns neighbouts to intersections based on a predefined pattern.
     */
    public void assignNeighboursToIntersections() {
        intersections.get(0).setNeighbours(null, intersections.get(1), intersections.get(8));
        intersections.get(1).setNeighbours(intersections.get(0), intersections.get(2), null);
        intersections.get(2).setNeighbours(intersections.get(1), intersections.get(3), intersections.get(10));
        intersections.get(3).setNeighbours(intersections.get(2), intersections.get(4), null);
        intersections.get(4).setNeighbours(intersections.get(3), intersections.get(5), intersections.get(12));
        intersections.get(5).setNeighbours(intersections.get(4), intersections.get(6), null);
        intersections.get(6).setNeighbours(intersections.get(5), null, intersections.get(14));
        intersections.get(7).setNeighbours(null, intersections.get(8), intersections.get(17));
        intersections.get(8).setNeighbours(intersections.get(7), intersections.get(9), intersections.get(0));
        intersections.get(9).setNeighbours(intersections.get(8), intersections.get(10), intersections.get(19));
        intersections.get(10).setNeighbours(intersections.get(9), intersections.get(11), intersections.get(2));
        intersections.get(11).setNeighbours(intersections.get(10), intersections.get(12), intersections.get(21));
        intersections.get(12).setNeighbours(intersections.get(11), intersections.get(13), intersections.get(4));
        intersections.get(13).setNeighbours(intersections.get(12), intersections.get(14), intersections.get(23));
        intersections.get(14).setNeighbours(intersections.get(13), null, intersections.get(6));
        intersections.get(15).setNeighbours(null, intersections.get(16), intersections.get(25));
        intersections.get(16).setNeighbours(intersections.get(15), intersections.get(17), intersections.get(27));
        intersections.get(17).setNeighbours(intersections.get(16), intersections.get(18), intersections.get(7));
        intersections.get(18).setNeighbours(intersections.get(17), intersections.get(19), intersections.get(29));
        intersections.get(19).setNeighbours(intersections.get(18), intersections.get(20), intersections.get(9));
        intersections.get(20).setNeighbours(intersections.get(19), intersections.get(21), intersections.get(31));
        intersections.get(21).setNeighbours(intersections.get(20), intersections.get(22), intersections.get(11));
        intersections.get(22).setNeighbours(intersections.get(21), intersections.get(23), intersections.get(33));
        intersections.get(23).setNeighbours(intersections.get(22), intersections.get(24), intersections.get(13));
        intersections.get(24).setNeighbours(intersections.get(23), intersections.get(25), intersections.get(35));
        intersections.get(25).setNeighbours(intersections.get(24), intersections.get(26), intersections.get(15));
        intersections.get(26).setNeighbours(intersections.get(25), null, intersections.get(37));
        intersections.get(27).setNeighbours(null, intersections.get(28), intersections.get(16));
        intersections.get(28).setNeighbours(intersections.get(27), intersections.get(29), intersections.get(38));
        intersections.get(29).setNeighbours(intersections.get(28), intersections.get(30), intersections.get(18));
        intersections.get(30).setNeighbours(intersections.get(29), intersections.get(31), intersections.get(40));
        intersections.get(31).setNeighbours(intersections.get(30), intersections.get(32), intersections.get(20));
        intersections.get(32).setNeighbours(intersections.get(31), intersections.get(33), intersections.get(42));
        intersections.get(33).setNeighbours(intersections.get(32), intersections.get(34), intersections.get(22));
        intersections.get(34).setNeighbours(intersections.get(33), intersections.get(35), intersections.get(44));
        intersections.get(35).setNeighbours(intersections.get(34), intersections.get(36), intersections.get(24));
        intersections.get(36).setNeighbours(intersections.get(35), intersections.get(37), intersections.get(46));
        intersections.get(37).setNeighbours(intersections.get(36), null, intersections.get(26));
        intersections.get(38).setNeighbours(null, intersections.get(39), intersections.get(28));
        intersections.get(39).setNeighbours(intersections.get(38), intersections.get(40), intersections.get(47));
        intersections.get(40).setNeighbours(intersections.get(39), intersections.get(41), intersections.get(30));
        intersections.get(41).setNeighbours(intersections.get(40), intersections.get(42), intersections.get(49));
        intersections.get(42).setNeighbours(intersections.get(41), intersections.get(43), intersections.get(32));
        intersections.get(43).setNeighbours(intersections.get(42), intersections.get(44), intersections.get(51));
        intersections.get(44).setNeighbours(intersections.get(43), intersections.get(45), intersections.get(34));
        intersections.get(45).setNeighbours(intersections.get(44), intersections.get(46), intersections.get(53));
        intersections.get(46).setNeighbours(intersections.get(45), intersections.get(47), intersections.get(36));
        intersections.get(47).setNeighbours(null, intersections.get(48), intersections.get(39));
        intersections.get(48).setNeighbours(intersections.get(47), intersections.get(49), null);
        intersections.get(49).setNeighbours(intersections.get(48), intersections.get(50), intersections.get(41));
        intersections.get(50).setNeighbours(intersections.get(49), intersections.get(51), null);
        intersections.get(51).setNeighbours(intersections.get(50), intersections.get(52), intersections.get(43));
        intersections.get(52).setNeighbours(intersections.get(51), intersections.get(53), null);
        intersections.get(53).setNeighbours(intersections.get(52), null, intersections.get(45));
    }

    /**
     * Assigns ends to roads based on the intersections.
     * Each road is created by connecting two intersections.
     */
    public void assignEndsToRoads() {
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(0), intersections.get(1)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(1), intersections.get(2)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(2), intersections.get(3)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(3), intersections.get(4)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(4), intersections.get(5)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(5), intersections.get(6)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(0), intersections.get(8)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(2), intersections.get(10)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(4), intersections.get(12)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(6), intersections.get(14)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(7), intersections.get(8)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(8), intersections.get(9)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(9), intersections.get(10)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(10), intersections.get(11)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(11), intersections.get(12)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(12), intersections.get(13)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(13), intersections.get(14)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(14), intersections.get(15)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(7), intersections.get(17)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(9), intersections.get(19)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(11), intersections.get(21)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(13), intersections.get(23)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(15), intersections.get(25)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(16), intersections.get(17)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(17), intersections.get(18)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(18), intersections.get(19)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(19), intersections.get(20)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(20), intersections.get(21)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(21), intersections.get(22)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(22), intersections.get(23)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(23), intersections.get(24)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(24), intersections.get(25)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(25), intersections.get(26)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(16), intersections.get(27)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(18), intersections.get(29)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(20), intersections.get(31)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(22), intersections.get(33)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(24), intersections.get(35)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(26), intersections.get(37)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(27), intersections.get(28)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(28), intersections.get(29)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(29), intersections.get(30)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(30), intersections.get(31)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(31), intersections.get(32)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(32), intersections.get(33)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(33), intersections.get(34)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(34), intersections.get(35)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(35), intersections.get(36)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(36), intersections.get(37)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(28), intersections.get(38)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(30), intersections.get(40)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(32), intersections.get(42)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(34), intersections.get(44)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(36), intersections.get(46)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(38), intersections.get(39)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(39), intersections.get(40)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(40), intersections.get(41)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(41), intersections.get(42)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(42), intersections.get(43)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(43), intersections.get(44)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(44), intersections.get(45)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(45), intersections.get(46)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(39), intersections.get(47)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(41), intersections.get(49)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(43), intersections.get(51)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(45), intersections.get(53)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(47), intersections.get(48)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(48), intersections.get(49)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(49), intersections.get(50)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(50), intersections.get(51)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(51), intersections.get(52)))));
        roads.add(new Road(new HashSet<>(Arrays.asList(intersections.get(52), intersections.get(53)))));
    }

}

