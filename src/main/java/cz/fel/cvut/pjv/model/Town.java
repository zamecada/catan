package cz.fel.cvut.pjv.model;

public class Town implements BuildStructure {

    private Player player;

    public Town(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }
    @Override
    public int getResourceMultiplier() {
        // Towns have a resource multiplier of 2
        return 2;
    }
}
