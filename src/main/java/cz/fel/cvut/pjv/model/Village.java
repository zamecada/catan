package cz.fel.cvut.pjv.model;

public class Village implements BuildStructure {

    private Player player;

    public Village(Player player) {
        this.player = player;
    }

    @Override
    public int getResourceMultiplier() {
        // Villages have a resource multiplier of 1
        return 1;
    }

    public Player getPlayer() {
        return player;
    }
}
