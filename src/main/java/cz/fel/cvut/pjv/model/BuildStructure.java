package cz.fel.cvut.pjv.model;

public interface BuildStructure {
    Player getPlayer();
    int getResourceMultiplier();
}