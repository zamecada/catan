package cz.fel.cvut.pjv.view;

import java.util.List;
import java.util.Random;

import cz.fel.cvut.pjv.controller.Game;
import cz.fel.cvut.pjv.model.Player;
import cz.fel.cvut.pjv.model.Terrain;
import cz.fel.cvut.pjv.model.Tile;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

public class BoardUI{

    private Game game;
    
    //Activation Numbers
    @FXML
    private Text Num1, Num2, Num3, Num4, Num5, Num6, Num7, Num8, Num9, Num10, Num11, Num12, Num13, Num14, Num15, Num16, Num17, Num18, Num19, AmountOfOre, AmountOfWool, AmountOfGrain, AmountOfBrick, AmountOfLumber, GameRound, PlayersTurn, Player1Name, Player1Points, Player2Name, Player2Points, DiceOutcome, Dice1, Dice2, Note, Error;
    private List<Text> activationNumberLabels;
    //Tiles
    @FXML
    private Polygon Hex1, Hex2, Hex3, Hex4, Hex5, Hex6, Hex7, Hex8, Hex9, Hex10, Hex11, Hex12, Hex13, Hex14, Hex15, Hex16, Hex17, Hex18, Hex19;
    private List<Polygon> hexes;
    //Intersections
    @FXML
    private RadioButton Intersection1, Intersection2, Intersection3, Intersection4, Intersection5, Intersection6, Intersection7, Intersection8, Intersection9, Intersection10, Intersection11, Intersection12, Intersection13, Intersection14, Intersection15, Intersection16, Intersection17, Intersection18, Intersection19, Intersection20, Intersection21, Intersection22, Intersection23, Intersection24, Intersection25, Intersection26, Intersection27, Intersection28, Intersection29, Intersection30, Intersection31, Intersection32, Intersection33, Intersection34, Intersection35, Intersection36, Intersection37, Intersection38, Intersection39, Intersection40, Intersection41, Intersection42, Intersection43, Intersection44, Intersection45, Intersection46, Intersection47, Intersection48, Intersection49, Intersection50, Intersection51, Intersection52, Intersection53, Intersection54;
    private List<RadioButton> intersectionButtons;
    //Roads
    @FXML
    private Rectangle Road1, Road2, Road3, Road4, Road5, Road6, Road7, Road8, Road9, Road10, Road11, Road12, Road13, Road14, Road15, Road16, Road17, Road18, Road19, Road20, Road21, Road22, Road23, Road24, Road25, Road26, Road27, Road28, Road29, Road30, Road31, Road32, Road33, Road34, Road35, Road36, Road37, Road38, Road39, Road40, Road41, Road42, Road43, Road44, Road45, Road46, Road47, Road48, Road49, Road50, Road51, Road52, Road53, Road54, Road55, Road56, Road57, Road58, Road59, Road60, Road61, Road62, Road63, Road64, Road65, Road66, Road67, Road68, Road69, Road70, Road71, Road72;
    private List<Rectangle> roadRectangles;
    //Buttons
    @FXML
    private Button Roll, NextTurn, QuitGame;

    public BoardUI() {
        hexes = List.of(Hex1, Hex2, Hex3, Hex4, Hex5, Hex6, Hex7, Hex8, Hex9, Hex10, Hex11, Hex12, Hex13, Hex14, Hex15, Hex16, Hex17, Hex18, Hex19);
        activationNumberLabels = List.of(Num1, Num2, Num3, Num4, Num5, Num6, Num7, Num8, Num9, Num10, Num11, Num12, Num13, Num14, Num15, Num16, Num17, Num18, Num19);
        intersectionButtons = List.of(Intersection1, Intersection2, Intersection3, Intersection4, Intersection5, Intersection6, Intersection7, Intersection8, Intersection9, Intersection10, Intersection11, Intersection12, Intersection13, Intersection14, Intersection15, Intersection16, Intersection17, Intersection18, Intersection19, Intersection20, Intersection21, Intersection22, Intersection23, Intersection24, Intersection25, Intersection26, Intersection27, Intersection28, Intersection29, Intersection30, Intersection31, Intersection32, Intersection33, Intersection34, Intersection35, Intersection36, Intersection37, Intersection38, Intersection39, Intersection40, Intersection41, Intersection42, Intersection43, Intersection44, Intersection45, Intersection46, Intersection47, Intersection48, Intersection49, Intersection50, Intersection51, Intersection52, Intersection53, Intersection54);
        roadRectangles = List.of(Road1, Road2, Road3, Road4, Road5, Road6, Road7, Road8, Road9, Road10, Road11, Road12, Road13, Road14, Road15, Road16, Road17, Road18, Road19, Road20, Road21, Road22, Road23, Road24, Road25, Road26, Road27, Road28, Road29, Road30, Road31, Road32, Road33, Road34, Road35, Road36, Road37, Road38, Road39, Road40, Road41, Road42, Road43, Road44, Road45, Road46, Road47, Road48, Road49, Road50, Road51, Road52, Road53, Road54, Road55, Road56, Road57, Road58, Road59, Road60, Road61, Road62, Road63, Road64, Road65, Road66, Road67, Road68, Road69, Road70, Road71, Road72);

        initializeBoard();
    }

    public void setGame(Game game) {
        this.game = game;
    }

    private void initializeBoard() {
        Image fields = new Image(BoardUI.class.getResourceAsStream("imgs/fields.png"));
        Image hills = new Image(BoardUI.class.getResourceAsStream("imgs/hills.png"));
        Image pasture = new Image(BoardUI.class.getResourceAsStream("imgs/pasture.png"));
        Image forest = new Image(BoardUI.class.getResourceAsStream("imgs/forest.png"));
        Image mountains = new Image(BoardUI.class.getResourceAsStream("imgs/mountains.png"));
        Image desert = new Image(BoardUI.class.getResourceAsStream("imgs/desert.png"));
        List<Tile> tiles = game.getBoard().getTiles();

        for (int i = 0; i < hexes.size() ; i++) {
            switch (tiles.get(i).getTerrain()) {
                case FIELDS:
                    hexes.get(i).setFill(new ImagePattern(fields));
                    break;
                case HILLS:
                    hexes.get(i).setFill(new ImagePattern(hills));
                    break;
                case PASTURE:
                    hexes.get(i).setFill(new ImagePattern(pasture));
                    break;
                case FOREST:
                    hexes.get(i).setFill(new ImagePattern(forest));
                    break;
                case MOUNTAINS:
                    hexes.get(i).setFill(new ImagePattern(mountains));
                    break;
                case DESERT:
                    hexes.get(i).setFill(new ImagePattern(desert));
                    break;
            }
            activationNumberLabels.get(i).setText(String.valueOf(tiles.get(i).getActivationNumber()));
        }  
    }

    public void manageTurn(Player player) {
        PlayersTurn.setText(player.getName()+"'s turn");
        GameRound.setText(Integer.toString(game.getGameRound()));
        Player1Points.setText(String.valueOf(game.getPlayer1().getPoints()));
        Player2Points.setText(String.valueOf(game.getPlayer2().getPoints()));
        for (RadioButton button : intersectionButtons) {
            button.setDisable(true);
        }
        Roll.setDisable(true);
        NextTurn.setDisable(true);
        updateResources(player);
        // Perform actions based on the current game state
        switch (game.getGameState()) {
            case SETUP:
                setUpLabels();
                Note.setText("Set up the game");
                break;
            case ROLL:
                Roll.setDisable(false);
                Note.setText("Roll the dice");
                break;
            case BUILD:
                for (RadioButton button : intersectionButtons) {
                    button.setDisable(false);
                }
                NextTurn.setDisable(false);
                Note.setText("Build structures or end your turn");
                break;
        } 
    }



    public void updateResources(Player player){
        AmountOfOre.setText(String.valueOf(player.getResource("Ore")));
        AmountOfWool.setText(String.valueOf(player.getResource("Wool")));
        AmountOfGrain.setText(String.valueOf(player.getResource("Grain")));
        AmountOfBrick.setText(String.valueOf(player.getResource("Brick")));
        AmountOfLumber.setText(String.valueOf(player.getResource("Lumber")));
    }

    public void setUpLabels(){
        Error.setText("");
        Note.setText("Setting up the game");
        GameRound.setText(Integer.toString(game.getGameRound()));
        Player1Name.setText(game.getPlayer1().getName());
        Player1Points.setText(String.valueOf(game.getPlayer1().getPoints()));
        Player2Name.setText(game.getPlayer2().getName());
        Player2Points.setText(String.valueOf(game.getPlayer2().getPoints()));
        AmountOfOre.setText("0");
        AmountOfWool.setText("0");
        AmountOfGrain.setText("0");
        AmountOfBrick.setText("0");
        AmountOfLumber.setText("0");
        DiceOutcome.setText("");
        Dice1.setText("");
        Dice2.setText("");
    }

    public void rollDice() {
        int[] outcomes = game.getDice().roll();
        Dice1.setText(String.valueOf(outcomes[0]));
        Dice2.setText(String.valueOf(outcomes[1]));
        DiceOutcome.setText(String.valueOf(outcomes[0] + outcomes[1]));
        game.addResourcesToPlayers(outcomes[0] + outcomes[1]);
    }
    

    
}
