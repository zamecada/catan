
```
fiverr repo
├─ .vscode
│  ├─ launch.json
│  └─ settings.json
├─ pom.xml
├─ README.md
├─ src
│  └─ main
│     ├─ java
│     │  ├─ cz
│     │  │  └─ fel
│     │  │     └─ cvut
│     │  │        └─ pjv
│     │  │           ├─ CatanMain.java
│     │  │           ├─ controller
│     │  │           │  ├─ Game.java
│     │  │           │  ├─ GameSetup.java
│     │  │           │  └─ MainMenu.java
│     │  │           ├─ model
│     │  │           │  ├─ Board.java
│     │  │           │  ├─ BuildStructure.java
│     │  │           │  ├─ Dice.java
│     │  │           │  ├─ GameState.java
│     │  │           │  ├─ Intersection.java
│     │  │           │  ├─ Player.java
│     │  │           │  ├─ Road.java
│     │  │           │  ├─ Route.java
│     │  │           │  ├─ Terrain.java
│     │  │           │  ├─ Tile.java
│     │  │           │  ├─ Town.java
│     │  │           │  └─ Village.java
│     │  │           ├─ styles.css
│     │  │           └─ view
│     │  │              ├─ BoardUI.java
│     │  │              └─ test.java
│     │  └─ module-info.java
│     └─ resources
│        ├─ cz
│        │  └─ fel
│        │     └─ cvut
│        │        └─ pjv
│        │           ├─ controller
│        │           │  └─ fields.png
│        │           ├─ defaultIntersections.json
│        │           ├─ defaultTiles.json
│        │           ├─ GameBoard.fxml
│        │           ├─ GameSetup.fxml
│        │           ├─ MainMenu.fxml
│        │           ├─ primary.fxml
│        │           ├─ secondary.fxml
│        │           └─ view
│        │              └─ imgs
│        │                 ├─ ResourceCards.png
│        │                 ├─ Road.png
│        │                 ├─ Town.png
│        │                 └─ Village.png
│        ├─ desert.png
│        ├─ forest.png
│        ├─ hills.png
│        ├─ mountains.png
│        └─ pasture.png
└─ target
   ├─ classes
   │  ├─ cz
   │  │  └─ fel
   │  │     └─ cvut
   │  │        └─ pjv
   │  │           ├─ CatanMain.class
   │  │           ├─ controller
   │  │           │  ├─ fields.png
   │  │           │  ├─ Game.class
   │  │           │  ├─ GameSetup.class
   │  │           │  └─ MainMenu.class
   │  │           ├─ defaultIntersections.json
   │  │           ├─ defaultTiles.json
   │  │           ├─ GameBoard.fxml
   │  │           ├─ GameSetup.fxml
   │  │           ├─ MainMenu.fxml
   │  │           ├─ model
   │  │           │  ├─ Board.class
   │  │           │  ├─ BuildStructure.class
   │  │           │  ├─ Dice.class
   │  │           │  ├─ GameState.class
   │  │           │  ├─ Intersection.class
   │  │           │  ├─ Player.class
   │  │           │  ├─ Road.class
   │  │           │  ├─ Route.class
   │  │           │  ├─ Terrain.class
   │  │           │  ├─ Tile.class
   │  │           │  ├─ Town.class
   │  │           │  └─ Village.class
   │  │           ├─ primary.fxml
   │  │           ├─ secondary.fxml
   │  │           ├─ styles.css
   │  │           └─ view
   │  │              ├─ BoardUI.class
   │  │              ├─ imgs
   │  │              │  ├─ ResourceCards.png
   │  │              │  ├─ Road.png
   │  │              │  ├─ Town.png
   │  │              │  └─ Village.png
   │  │              └─ test.class
   │  ├─ desert.png
   │  ├─ forest.png
   │  ├─ hills.png
   │  ├─ module-info.class
   │  ├─ mountains.png
   │  └─ pasture.png
   └─ test-classes

```